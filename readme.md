Check if a password is part of the have-i-been-pwned db.

Requires downloading the file https://haveibeenpwned.com/Passwords which has the schema `hash:number`.

Had some trouble with the delimiter in my version of SQLite so to convert the text file into an actual csv:

	cat pwned-passwords-ordered-by-count.txt | tr : , > pwned-passwords-ordered-by-count.csv
	
And to get the data into SQLite:

```sql
.separator ","
create table pw(hash text, n int);
.import pwned-passwords-ordered-by-count.csv pw

create index pw_idx on pw (hash);
```

In case of out-of-memory errors on the `.import` step - use a newer version of SQLite.
