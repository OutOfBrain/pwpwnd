<?php

$hash = $_GET['hash'] ?? null;

if ($hash) {
	// check hash - return result
	$sqliteFile = 'pwned-passwords-ordered-by-count.db';
	$hits = 0;

	if (file_exists($sqliteFile)) {
		$db = new SQLite3($sqliteFile);
		$statement = $db->prepare('select n as hits from pw where hash = :hash');
		if ($statement === false) {
			echo($db->lastErrorMsg());
			die();
		}
		$statement->bindValue(':hash', strtoupper($hash), SQLITE3_TEXT);
		$result = $statement->execute();

		if ($result->numColumns() > 0) {
			$hits = $result->fetchArray()['hits'];
		}
	}

	echo(json_encode(['hits' => $hits]));
}
else {
	// regular get - return index
	include('index_template.html');
}
